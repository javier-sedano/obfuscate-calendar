#!/bin/sh -e
install -d build
zip build/obfuscate-calendar.zip css/* images/*.png manifest.json popup.html popup.js
unzip -l build/obfuscate-calendar.zip
