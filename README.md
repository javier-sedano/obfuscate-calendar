Chrome extension to obfuscate the details of Google Calendar appointments, so you can share a screenshot with strangers.

Page of the extension in the Chrome Web Store: https://chrome.google.com/webstore/detail/obfuscate-calendar/jnkpkbfoojjocendndbgepfhfgpafgkd?hl=en-GB
# Screenshot

![Screenshot-1](store/obfuscate-calendar-screenshot-1.jpg)

# FAQ

* **Google Calendar allows sharing the calendar without details with a given user... why not just using this?** Well... if it fits your needs... why not? However, with this extension you can send a screenshot, so the receviver does not need to use Google Calendar; and you can show (obfuscated) more than one calendar (you colleagues' calendars, your personal calendar,... see schreenshot).

# Useful links
* https://developer.chrome.com/docs/extensions/mv3/getstarted/
* https://chrome.google.com/webstore/devconsole/55ff31a4-5bac-4139-b9e0-5c6f28614b11
