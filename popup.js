toggle.addEventListener("click", async () => {
  const [tab] = await chrome.tabs.query({ active: true, currentWindow: true });

  chrome.scripting.executeScript({
    target: { tabId: tab.id },
    func: toggleObfuscation,
  });
});

function toggleObfuscation() {

  function createStyleHolder() {
    let styleHolder = document.getElementById('--style');
    if (styleHolder) {
      return;
    }
    styleHolder = document.createElement('style');
    styleHolder.id = '--style';
    styleHolder.innerHTML = '.--blur { -webkit-filter: blur(4px); filter: blur(4px); }\
                   .rsvp-no-chip.--hide, .rsvp-no-bg.--hide { opacity: 0; }';
    document.head.appendChild(styleHolder);
  }

  function getAppointmentElements() {
    // return document.querySelectorAll('div[role=presentation] div[role=presentation] div[role=presentation] span');
    return document.querySelectorAll('div[role=button] span');
  }

  function toggleBlur(nodeList) {
    nodeList.forEach(node => { node.classList.toggle('--blur') });
  }

  createStyleHolder();
  const appointmentElements = getAppointmentElements();
  toggleBlur(appointmentElements);
}
